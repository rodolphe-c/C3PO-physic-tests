# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr
# Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
# Copyright © 2015 Théo Torcq, ceto.rcq@gmail.com
https://gitlab.com/rodolphe-c/thoth-physic-lab


 ------------------
| Thōth Physic Lab |
 ------------------

This project brings together various aspects about the laws of physics.
Clone it to run the different exemples:

git clone https://gitlab.com/rodolphe-c/thoth-physic-lab
cd thoth-physic-lab


 --------------------
| System Requirement |
 --------------------

Required:
- hopp
- Thōth

Optional:
- aonl


 -------------
| Compilation |
 -------------

With CMake
----------

mkdir build
cd build
cmake ..
make
# make test
./project

Without CMake
-------------

Thōth is a header-only library, you can specify where are the include directory of Thōth to your IDE. (But you have to define some macros to enable optional parts.) Thōth have some dependancies with link (SFML and OGRE).
