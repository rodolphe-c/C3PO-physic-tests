#include <osg/Geode>
#include <osgViewer/Viewer>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/ShapeDrawable>
#include <osg/BlendFunc>
#include <osg/CullFace>
#include <osgGA/TrackballManipulator>

int main( /*int argc, char** argv*/ )
{
	osgViewer::Viewer viewer;
	viewer.setUpViewInWindow( 30, 30, 800, 450 );

	osg::ref_ptr<osg::Group> root = new osg::Group;
	//Cell
	{
		osg::ref_ptr<osg::Geode> cell = new osg::Geode;
		osg::ref_ptr<osg::ShapeDrawable> obj = new osg::ShapeDrawable;
		obj->setShape(new osg::Sphere(osg::Vec3(0.f, 0.f, 0.f), 20.f));
		obj->setColor(osg::Vec4(1.f, 0.4f, 0.f, 1.f));
		cell->addDrawable(obj.get());


		float Opacity=0.2f;
		osg::Material * material = new osg::Material();

		material->setTransparency(osg::Material::FRONT_AND_BACK, 1.f - Opacity);
		cell->getOrCreateStateSet()->setRenderBinDetails(0, "transparent");
		cell->getOrCreateStateSet()->setMode( GL_BLEND, osg::StateAttribute::ON );
		cell->getOrCreateStateSet()->setRenderingHint(osg::StateSet::TRANSPARENT_BIN);
		cell->getOrCreateStateSet()->setAttributeAndModes(material, osg::StateAttribute::OVERRIDE);
		cell->getOrCreateStateSet()->setAttributeAndModes(new osg::CullFace(osg::CullFace::FRONT));
		cell->getOrCreateStateSet()->setAttributeAndModes(new osg::CullFace(osg::CullFace::BACK));

		root->addChild(cell.get());
	}

	//Enzyme
	{
		osg::ref_ptr<osg::Geode> geode = new osg::Geode;
		osg::ref_ptr<osg::ShapeDrawable> obj = new osg::ShapeDrawable;
		obj->setShape(new osg::Sphere(osg::Vec3(3.f, 0.f, 0.f), 1.f));
		obj->setColor(osg::Vec4(1.f,0.f,0.f,1.f));
		geode->addDrawable(obj.get());

		osg::ref_ptr<osg::StateSet> stateSet = geode->getOrCreateStateSet();
		osg::Material* material = new osg::Material;
		material->setColorMode( osg::Material::DIFFUSE);
		stateSet->setAttribute(material);
		stateSet->setMode( GL_DEPTH_TEST, osg::StateAttribute::ON );

		root->addChild(geode.get());
	}
	viewer.setSceneData(root.get());

	viewer.setCameraManipulator( new osgGA::TrackballManipulator );
	while(!viewer.done())
	{
		viewer.frame();
	}

	return 0;
}
