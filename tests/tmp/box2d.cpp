#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.hpp>

/** We need this to easily convert between pixel and real-world coordinates*/
static const float SCALE = 60.f;

/** Create the base for the boxes to land */
void createGround(b2::World& World, float X, float Y);

/** Create the boxes */
void createBox(b2::World& World, int MouseX, int MouseY);

int main()
{
	/** Prepare the window */
	sf::RenderWindow window(sf::VideoMode(800, 600), "Test");
	window.setFramerateLimit(60);

	/** Prepare the world */
	b2::Vec2 Gravity(0.f, 9.8f);
	b2::World World(Gravity);
	createGround(World, 400.f, 500.f);

	/** Prepare textures */
	sf::Texture GroundTexture;
	sf::Texture BoxTexture;
	GroundTexture.loadFromFile("../img/ground.png");
	BoxTexture.loadFromFile("../img/box.png");

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// évènement "fermeture demandée" : on ferme la fenêtre
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			if (event.type != sf::Event::LostFocus)
			{
				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						int x = sf::Mouse::getPosition(window).x;
						int y = sf::Mouse::getPosition(window).y;
						createBox(World, x, y);
					}
				}
			}
		}
		World.Step(1/60.f, 8, 3);

		window.clear(sf::Color::White);

		int BodyCount = 0;
		for (b2::Body* BodyIterator = World.GetBodyList(); BodyIterator != 0; BodyIterator = BodyIterator->GetNext())
		{
			if (BodyIterator->GetType() == b2::dynamicBody)
			{
				sf::Sprite Sprite;
				Sprite.setTexture(BoxTexture);
				Sprite.setOrigin(16.f, 16.f);
				Sprite.setPosition(SCALE * BodyIterator->GetPosition().x, SCALE * BodyIterator->GetPosition().y);
				Sprite.setRotation(BodyIterator->GetAngle() * 180/b2::pi);
				window.draw(Sprite);
				++BodyCount;
			}
			else
			{
				sf::Sprite GroundSprite;
				GroundSprite.setTexture(GroundTexture);
				GroundSprite.setOrigin(400.f, 8.f);
				GroundSprite.setPosition(BodyIterator->GetPosition().x * SCALE, BodyIterator->GetPosition().y * SCALE);
				GroundSprite.setRotation(180/b2::pi * BodyIterator->GetAngle());
				window.draw(GroundSprite);
			}
		}
		window.display();
	}

	return 0;
}

void createBox(b2::World& World, int MouseX, int MouseY)
{
	b2::BodyDef BodyDef;
	BodyDef.position = b2::Vec2(MouseX/SCALE, MouseY/SCALE);
	BodyDef.type = b2::dynamicBody;
	b2::Body* Body = World.CreateBody(&BodyDef);

	b2::PolygonShape Shape;
	Shape.SetAsBox((32.f/2)/SCALE, (32.f/2)/SCALE);
	b2::FixtureDef FixtureDef;
	FixtureDef.density = 1.f;
	FixtureDef.friction = 0.7f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}

void createGround(b2::World& World, float X, float Y)
{
	b2::BodyDef BodyDef;
	BodyDef.position = b2::Vec2(X/SCALE, Y/SCALE);
	BodyDef.type = b2::staticBody;
	b2::Body* Body = World.CreateBody(&BodyDef);

	b2::PolygonShape Shape;
	Shape.SetAsBox((800.f/2)/SCALE, (16.f/2)/SCALE);
	b2::FixtureDef FixtureDef;
	FixtureDef.density = 0.f;
	FixtureDef.shape = &Shape;
	Body->CreateFixture(&FixtureDef);
}
