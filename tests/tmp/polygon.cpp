// Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2015 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <thread>

#include "c3po/2D/polygon.hpp"
#include "c3po/2D/geometry.hpp"

int main()
{
	int const height (1200);
	int const width (2000);

	std::vector<sf::Vector2f> traces;
	std::vector<sf::CircleShape> circles;
	std::vector<polygon> convexes;

	// Movable Polygon
	std::vector<sf::Vector2f> points1 {{400.f,300.f}, {500.f,300.f}, {450.f,200.f}};
	polygon p1(points1);
	p1.setFillColor(sf::Color::Green);

	// Window
	sf::RenderWindow window(sf::VideoMode(width, height), "Polygon");

	// Render
	bool is_open = true;
	auto render = [&]()
	{
		while (is_open)
		{
			window.clear();
			for (auto const & p : convexes)
			{
				window.draw(p);
			}

			for (auto const & c : circles)
			{
				window.draw(c);
			}
			window.draw(p1);
			window.display();
		}
	};

	std::thread th(render);

	sf::Clock clock;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// évènement "fermeture demandée" : on ferme la fenêtre
			if (event.type == sf::Event::Closed)
			{
				is_open = false;
				th.join();
				window.close();
			}
			if (event.type != sf::Event::LostFocus)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{
					circles.clear();
					traces.clear();
					convexes.clear();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
				{
					if(traces.size() >= 3)
					{
						circles.clear();
						convexes.push_back(polygon(traces));
						traces.clear();
					}
				}
				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Right)
					{
						sf::CircleShape c (5);
						c.setPosition(float(sf::Mouse::getPosition(window).x), float(sf::Mouse::getPosition(window).y));
						c.setFillColor(sf::Color::Red);
						circles.push_back(c);
						traces.push_back({float(sf::Mouse::getPosition(window).x), float(sf::Mouse::getPosition(window).y)});
					}
				}
			}
		}

		// Time
		float const elapsed = clock.restart().asSeconds();
		float const moving = 150 * elapsed;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			p1.move(-moving, 0);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			p1.move(moving, 0);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			p1.move(0, -moving);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			p1.move(0, moving);
		}

		for (polygon & p : convexes)
		{
			if(overlaps(p, p1))
			{
				p1.setFillColor(sf::Color::Red);
				p.setFillColor(sf::Color::Red);
			}
			else
			{
				p1.setFillColor(sf::Color::Green);
				p.setFillColor(sf::Color::Green);
			}
		}
	}

	return 0;
}
