// Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2015 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <thread>

#include "c3po/2D/entity.hpp"
#include "c3po/2D/function.hpp"
#include "c3po/quantum_mechanics.hpp"

int main()
{
	int const coeff (10);
	int const height (1200);
	int const width (2000);
	float const size (40.f);

	int i (0);
	bool planet_mode (true);

	// Window
	sf::RenderWindow window(sf::VideoMode(width, height), "Force Field");
	std::vector<c3po::entity_circle<float>> entities;
	std::vector<c3po::entity_circle<float>> m_planets;
	std::vector<sf::CircleShape> m_cercles;
	sf::Clock update;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// évènement "fermeture demandée" : on ferme la fenêtre
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			if (event.type != sf::Event::LostFocus)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{
					m_cercles.clear();
					entities.clear();
					m_planets.clear();
					i = 0;
				}

				if (event.type == sf::Event::KeyPressed)
				{
					if (event.key.code == sf::Keyboard::P)
					{
						planet_mode = !planet_mode;
						std::cout << planet_mode << std::endl;
					}
				}

				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						if (planet_mode)
						{
							sf::Vector2f pos (float(sf::Mouse::getPosition(window).x), float(sf::Mouse::getPosition(window).y));
							c3po::entity_circle<float> e (size, pos, pos);
							e.color_background = hopp::color::white();
							m_planets.push_back(e);
						}
						else
						{
							if (i == 2)
							{
								m_cercles.clear();
								i = 0;
							}
							sf::CircleShape c (5);
							c.setPosition(float(sf::Mouse::getPosition(window).x), float(sf::Mouse::getPosition(window).y));

							m_cercles.push_back(c);

							i++;

							if (i == 2)
							{
								entities.push_back(c3po::entity_circle<float>( size,m_cercles.at(0).getPosition(), m_cercles.at(1).getPosition()));
							}
						}
					}
				}
			}
		}

		sf::Time t1 = update.getElapsedTime();

		/* ******************* Gravity ******************* */
		if(t1.asMilliseconds() > 1)
		{
			update.restart();

			float const time = t1.asSeconds();

			for(std::size_t i = 0; i < entities.size(); i++)
			{
				/*
				if(entities[i].position().y <= 0 || entities[i].position().y >= height - size/2)
				{
					//Mise à jour de vy
					entities[i].v.y = -entities[i].v.y;
					entities[i].set_position(entities[i].back_pos);
				}

				if(entities[i].position().x >= width - size/2 || entities[i].position().x <= 0)
				{
					//Mise à jour de vx
					entities[i].v.x = -entities[i].v.x;
					entities[i].set_position(entities[i].back_pos);
				}
				*/

				hopp::vector2<float> F (0,0);
				for (auto p : m_planets)
				{
					F = c3po::force  (
					                    F,
					                    entities[i].position(),
					                    entities[i].mass,
					                    p.position(),
					                    p.mass* coeff,
					                    -0.9
					            );
				}

				for (size_t j = 0;j < entities.size(); ++j)
				{
					if (i != j)
					{
						F = c3po::force  (
						                    F,
						                    entities[i].position(),
						                    entities[i].mass,
						                    entities[j].position(),
						                    entities[j].mass* coeff,
						                    -0.9
						                );
					}
				}

				c3po::apply_force(F, entities[i].a, entities[i].v, entities[i].mass, time);

				//Appel de la fonction
				collision(i, entities);

				//entities[i].set_position(c3po::update(entities[i].position(), entities[i].v, time));
				entities[i].set_position(c3po::update(entities[i].position(), entities[i].v));
			}
		}

		/* ***************** Draw ***************** */
		window.clear();

		for (auto const & e : m_planets)
		{
			sf::CircleShape s(e.shape.radius);
			s.setOrigin(e.shape.radius,e.shape.radius);
			s.setPosition(e.position().x, e.position().y);
			window.draw(s);
		}

		for (auto e : entities)
		{
			sf::CircleShape s(e.shape.radius);
			s.setOrigin(e.shape.radius,e.shape.radius);
			s.setPosition(e.position().x, e.position().y);
			s.setFillColor(to_sfml(e.color_background));
			//std::cout << e.position() << std::endl;
			window.draw(s);
		}

		window.display();
	}

	return 0;
}
