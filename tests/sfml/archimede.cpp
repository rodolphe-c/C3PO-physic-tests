// Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2015 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <thread>

#include "c3po/2D/entity.hpp"
#include "c3po/2D/function.hpp"
#include "c3po/quantum_mechanics.hpp"

using vector2f = hopp::vector2<float>;


int main()
{
	int const height (1200);
	int const width (2000);
	float const size (40.f);
	float const g (c3po::gravity_earth()*2);

	int i (0);
	float k (0.001f);
	float rho_f;

	// Window
	sf::RenderWindow window(sf::VideoMode(width, height), "Archimede");
	std::vector<c3po::entity_circle<float>> entities;
	std::vector<sf::CircleShape> m_cercles;
	sf::Clock update;
	sf::RectangleShape line(sf::Vector2f(width, 5));
	sf::RectangleShape fond(sf::Vector2f(width, height/2));


	line.setPosition(0, height/2);
	line.setFillColor(sf::Color(0,0,255, 100));
	fond.setFillColor(sf::Color(200, 200, 200));
	fond.setPosition(0,0);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// évènement "fermeture demandée" : on ferme la fenêtre
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			if (event.type != sf::Event::LostFocus)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{
					m_cercles.clear();
					entities.clear();
					i = 0;
				}

				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						if (i == 2)
						{
							m_cercles.clear();
							i = 0;
						}
						sf::CircleShape c (5);
						c.setFillColor(sf::Color(255,0,0,120));
						c.setPosition(float(sf::Mouse::getPosition(window).x), float(sf::Mouse::getPosition(window).y));

						m_cercles.push_back(c);

						i++;

						if (i == 2)
						{
							entities.push_back(c3po::entity_circle<float>(size, m_cercles.at(0).getPosition(), m_cercles.at(1).getPosition()));
						}
					}
				}
			}
		}

		sf::RectangleShape middle (sf::Vector2f(1,width));

		middle.setPosition(sf::Vector2f(0, height/2));

		sf::Time t1 = update.getElapsedTime();

		/* ******************* Gravity ******************* */
		if(t1.asMilliseconds() > 1)
		{
			update.restart();

			float const time = t1.asSeconds();

			for(std::size_t i = 0; i < entities.size(); i++)
			{
				if(entities[i].position().y > height/2)
				{
					rho_f = c3po::rho_water();
					k = 5.f;
				}
				else
				{
					rho_f = c3po::rho_air();
					k = 0.001f;
				}

				if(entities[i].position().y - size/2 < 0)
				{
					//Mise à jour de vy
					entities[i].v.y = -entities[i].v.y;
					auto pos = vector2f (entities[i].position().x, entities[i].position().y + size/2);
					entities[i].set_position(pos);
				}

				else if(entities[i].position().y + size/2 > height)
				{
					//Mise à jour de vy
					entities[i].v.y = -entities[i].v.y;
					auto pos = vector2f (entities[i].position().x, entities[i].position().y - size/2);
					entities[i].set_position(pos);
				}

				if(entities[i].position().x + size/2 > width)
				{
					//Mise à jour de vx
					entities[i].v.x = -entities[i].v.x;
					auto pos = vector2f (entities[i].position().x - size/2, entities[i].position().y);
					entities[i].set_position(pos);
				}

				else if (entities[i].position().x - size/2 < 0)
				{
					//Mise à jour de vx
					entities[i].v.x = -entities[i].v.x;
					auto pos = vector2f (entities[i].position().x + size/2, entities[i].position().y);
					entities[i].set_position(pos);
				}

				hopp::vector2<float> F (0,0);
				F = c3po::archimede(F, entities[i].mass, entities[i].rho, rho_f, g);
				F = c3po::gravity(F, entities[i].mass, g);
				F = c3po::fluid_resistance(F, entities[i].v, k);

				//Appel de la fonction

				c3po::apply_force(F, entities[i].a, entities[i].v, entities[i].mass, time);
				collision(i, entities);
				entities[i].set_position(c3po::update(entities[i].position(), entities[i].v));
			}
		}

		/* ***************** Draw ***************** */
		window.clear(sf::Color::Cyan);

		window.draw(fond);

		for (auto e : entities)
		{
			sf::CircleShape s(e.shape.radius);
			s.setOrigin(e.shape.radius,e.shape.radius);
			s.setPosition(e.position().x, e.position().y);
			s.setFillColor(to_sfml(e.color_background));
			//std::cout << e.position() << std::endl;
			window.draw(s);
		}

		for	(auto c : m_cercles)
		{
			window.draw(c);
		}

		window.draw(line);

		window.display();
	}

	return 0;
}
