
// Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2015 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <thread>

#include "hopp/geometry.hpp"

#include "c3po/2D/entity.hpp"
#include "c3po/2D/function.hpp"
#include "c3po/quantum_mechanics.hpp"

using vector2f = hopp::vector2<float>;

int main()
{
	int const height (1200);
	int const width (2000);
	float const size (40.f);
	float const g (c3po::gravity_earth());

	int i (0);

	// Window
	sf::RenderWindow window(sf::VideoMode(width, height), "Gravity");
	std::vector<c3po::entity_circle<float>> entities;
	std::vector<sf::CircleShape> circles;
	sf::Clock update;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// évènement "fermeture demandée" : on ferme la fenêtre
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			if (event.type != sf::Event::LostFocus)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::O))
				{
					circles.clear();
					entities.clear();
					i = 0;
				}

				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (event.mouseButton.button == sf::Mouse::Left)
					{
						if (i == 2)
						{
							circles.clear();
							i = 0;
						}
						sf::CircleShape c (5);
						c.setPosition(float(sf::Mouse::getPosition(window).x), float(sf::Mouse::getPosition(window).y));

						circles.push_back(c);

						i++;

						if (i == 2)
						{
							entities.push_back(c3po::entity_circle<float>(size, circles.at(0).getPosition(), circles.at(1).getPosition()));
						}
					}
				}
			}
		}

		sf::Time t1 = update.getElapsedTime();

		/* ******************* Gravity ******************* */
		if(t1.asMilliseconds() > 10)
		{
			update.restart();
			float const time = t1.asSeconds();

			for(size_t i = 0; i < entities.size(); i++)
			{
				if(entities[i].position().y - size/2 < 0)
				{
					//Mise à jour de vy
					entities[i].v.y = -entities[i].v.y;
					auto pos = vector2f (entities[i].position().x, entities[i].position().y + size/2);
					entities[i].set_position(pos);
				}

				else if(entities[i].position().y + size/2 > height)
				{
					//Mise à jour de vy
					entities[i].v.y = -entities[i].v.y;
					auto pos = vector2f (entities[i].position().x, entities[i].position().y - size/2);
					entities[i].set_position(pos);
				}

				if(entities[i].position().x + size/2 > width)
				{
					//Mise à jour de vx
					entities[i].v.x = -entities[i].v.x;
					auto pos = vector2f (entities[i].position().x - size/2, entities[i].position().y);
					entities[i].set_position(pos);
				}

				else if (entities[i].position().x - size/2 < 0)
				{
					//Mise à jour de vx
					entities[i].v.x = -entities[i].v.x;
					auto pos = vector2f (entities[i].position().x + size/2, entities[i].position().y);
					entities[i].set_position(pos);
				}

				vector2f F(0.f,0.f);
				F = c3po::gravity(F, entities[i].mass, g);
				c3po::apply_force(F, entities[i].a, entities[i].v, entities[i].mass, time);

				//Appel de la fonction
				c3po::collision(i,entities);
				entities[i].set_position(c3po::update(entities[i].position(), entities[i].v));
			}
		}

		/* ***************** Draw ***************** */
		window.clear();

		for (auto e : entities)
		{
			sf::CircleShape s(e.shape.radius);
			s.setOrigin(e.shape.radius,e.shape.radius);
			s.setPosition(e.position().x, e.position().y);
			s.setFillColor(to_sfml(e.color_background));
			//std::cout << e.position() << std::endl;
			window.draw(s);
		}

		for	(auto c : circles)
		{
			window.draw(c);
		}
		window.display();
	}

	return 0;
}
