#include <iostream>

#include <SFML/Graphics.hpp>


#include <c3po/quantum_mechanics.hpp>

int main()
{
	// création de la fenêtre
	float const height = 600;
	float const width = 800;
	sf::RenderWindow window(sf::VideoMode(800, 600), "My window");

	float const size (10.f);
	sf::CircleShape c (size);
	c.setPosition(400,300);
	float const mass (1.f);
	hopp::vector2<float> v (0,0);
	hopp::vector2<float> a (0,0);

	sf::Clock clock;

	const float fps = 100000;
	const float dt = 1 / fps;
	float accumulator = 0;
	float frame_start = clock.getElapsedTime().asSeconds();
	// on fait tourner le programme tant que la fenêtre n'a pas été fermée
	while (window.isOpen())
	{
		float const current_time = clock.getElapsedTime().asSeconds();

		accumulator+= current_time - frame_start;
		frame_start = current_time;

		// on traite tous les évènements de la fenêtre qui ont été générés depuis la dernière itération de la boucle
		sf::Event event;
		while (window.pollEvent(event))
		{
			// fermeture de la fenêtre lorsque l'utilisateur le souhaite
			if (event.type == sf::Event::Closed)
				window.close();
		}

		while (accumulator > dt)
		{
			if(c.getPosition().y - size < 0)
			{
				//Mise à jour de vy
				v.y = -v.y;
				auto pos = sf::Vector2f (c.getPosition().x, c.getPosition().y + size);
				c.setPosition(pos);
			}

			else if(c.getPosition().y + size > height)
			{
				//Mise à jour de vy
				v.y = -v.y;
				auto pos = sf::Vector2f (c.getPosition().x, c.getPosition().y - size);
				c.setPosition(pos);
			}

			if(c.getPosition().x + size > width)
			{
				//Mise à jour de vx
				v.x = -v.x;
				auto pos = sf::Vector2f (c.getPosition().x - size, c.getPosition().y);
				c.setPosition(pos);
			}

			else if (c.getPosition().x - size < 0)
			{
				//Mise à jour de vx
				v.x = -v.x;
				auto pos = sf::Vector2f (c.getPosition().x + size, c.getPosition().y);
				c.setPosition(pos);
			}

			sf::Vector2f F(0.f,0.0098f);
			a.x = F.x/mass;
			a.y = F.y/mass;

			v.y = v.y + a.y * dt;
			v.x = v.x + a.x * dt;

			c.move(v.x, v.y);
			accumulator -= dt;
		}

		// effacement de la fenêtre en noir
		window.clear(sf::Color::Black);

		// c'est ici qu'on dessine tout
		window.draw(c);

		// fin de la frame courante, affichage de tout ce qu'on a dessiné
		window.display();
	}

	return 0;
}
