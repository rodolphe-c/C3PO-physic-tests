Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr <br />
Copyright © 2015 - 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com <br />
Copyright © 2015 Théo Torcq, ceto.rcq@gmail.com <br />
https://gitlab.com/rodolphe-c/thoth-physic-lab

# C3PO physic tests

This project brings together various aspects about the laws of physics. <br />
Clone it to run the different exemples:

```sh
git clone https://gitlab.com/rodolphe-c/C3PO-physic-tests
cd C3PO-physic-tests
```

### System Requirement

Required:

- C++11 compiler

Optional:

- SFML
- OpenSceneGraph

### Compilation

###### With CMake

```sh
mkdir build
cd build
cmake ..
make
./project
```
