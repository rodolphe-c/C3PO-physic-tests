// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2017 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef QUANTUM_MECHANICS_HPP
#define QUANTUM_MECHANICS_HPP

#include <cmath>

#include "hopp/container.hpp"

namespace c3po
{
	template <class T = long>
	constexpr T ua() {return T(149597870000);}

	template <class T = float>
	constexpr T gravity_earth() {return T(9.80665);}

	template <class T = float>
	constexpr T gravity_mars() {return T(3.711);}

	template <class T = float>
	constexpr T gravity_mercure() {return T(3.701);}

	template <class T = float>
	constexpr T gravity_venus() {return T(8.87);}

	template <class T = float>
	constexpr T k_air() {return T(0.000018);}

	template <class T = float>
	constexpr T k_water() {return T(0.001);}

	template <class T = float>
	constexpr T rho_air() {return T(1.204);}

	template <class T = float>
	constexpr T rho_water() {return T(1000.0);}

	template <class T = double>
	constexpr T void_permitivity() {return T(0.00000000000885418782);}


	template<class T>
	inline
	T potential_electric(T const & q, T const & r, T const & epsilon = T(c3po::void_permitivity()))
	{
		return T(q/(4*M_PI*epsilon*r));
	}

	template<class vector2_t, class vector2_t0>
	inline
	vector2_t update(vector2_t const & pos, vector2_t0 const & v)
	{
		vector2_t F;
		F.x = pos.x + v.x;
		F.y = pos.y + v.y;
		return F;
	}

	template<class vector2_t0, class vector2_t1, class vector2_t, class mass_t, class time_t>
	inline
	void apply_force(vector2_t const & F, vector2_t0 & a, vector2_t1 & v, mass_t const & mass, time_t const & time)
	{
		a.x = F.x/mass;
		a.y = F.y/mass;

		v.y = v.y + a.y * time;
		v.x = v.x + a.x * time;
	}

	template<class vector2_t, class T>
	inline
	vector2_t gradient(vector2_t X, vector2_t Y, T const & potential)
	{
		vector2_t F;
		F.x = (potential*X.y - potential*X.x)/2;
		F.y = (potential*Y.y - potential*Y.x)/2;
		return F;
	}

	template<class vector2_t, class T>
	inline
	vector2_t force(vector2_t const & gradient, T const & q)
	{
		vector2_t F;
		F.x = -(q*gradient.x);
		F.y = -(q*gradient.y);
		return F;
	}

	template<class vector2_t0, class T0, class vector2_t2, class T1, class vector2_t, class T2>
	inline
	vector2_t force (vector2_t const & f, vector2_t0 const & p1, T0 const & m1, vector2_t2 const & p2, T1 const & m2, T2 const & p = T2(-2))
	{
		using fx_t = decltype(f.x);
		using fy_t = decltype(f.x);

		double Rx = double (p2.x-p1.x);
		double Ry = double (p2.y-p1.y);

		double R = std::sqrt(std::pow(p2.x-p1.x,2)+std::pow(p2.y-p1.y,2));
		double theta;

		if(Rx>0 && Ry>=0) {theta = std::atan(Ry/Rx);}
		else if(Rx>0 && Ry<0) {theta = std::atan(Ry/Rx) + 2*M_PI;}
		else if(Rx<0) {theta = std::atan(Ry/Rx) + M_PI;}
		else if(Rx==0 && Ry>0) {theta = M_PI/2.f;}
		else {theta = (3.f*M_PI)/2.f;}

		vector2_t F;
		F.x = f.x + fx_t((std::pow(R,p)* m1 * m2) * std::cos(theta));
		F.y = f.y + fy_t((std::pow(R,p)* m1 * m2) * std::sin(theta));
		return F;
	}

	// frottement
	template<class vector2_t, class vector2_t0, class T>
	inline
	vector2_t fluid_resistance (vector2_t const & f, vector2_t0 const & v, T const & k)
	{
		vector2_t F;
		F.x = f.x + -k*v.x;
		F.y = f.y + -k*v.y;
		return F;
	}

	// gravité
	template<class vector2_t, class T0, class T1>
	inline
	vector2_t gravity (vector2_t const & f, T0 const & mass, T1 const & g = T1(c3po::gravity_earth()))
	{
		vector2_t F;
		F.x = f.x;
		F.y = f.y + mass*g;
		return F;
	}

	// Archimede
	template<class vector2_t, class T0, class T1, class T2, class T3>
	inline
	vector2_t archimede (vector2_t const & f, T0 const & mass, T1 const & rho, T2 const & rho_f = T2(c3po::rho_water()), T3 const & g = T3(c3po::gravity_earth()))
	{
		vector2_t F;
		F.x = f.x;
		F.y = f.y + g*(-(mass/rho)*rho_f);
		return F;
	}

/*
	template<class vector2_t = hopp::vector2<float>>
	inline
	decltype(std::declval<vector2_t>()[0]) archimede (vector2_t const & f) //-> decltype(f.x)
	{
		vector2_t F;
		F.x = f.x;
		F.y = f.y + g*(-(mass/rho)*rho_f);
		return f.x;
	}

	template<class vector2_t = hopp::vector2<float>>
	inline
	auto const & archimede (vector2_t const & f = vector2_t(0,0)) -> decltype(f.x)
	{
		vector2_t F;
		F.x = f.x;
		F.y = f.y + g*(-(mass/rho)*rho_f);
		return F;
	}
*/
	//auto archimede (vector2_t const & f) -> decltype(f[0])
}

#endif
