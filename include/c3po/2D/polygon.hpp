// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2017 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef POLYGON_HPP
#define POLYGON_HPP

#include <SFML/Graphics.hpp>

class polygon : public sf::ConvexShape
{
public:
	template<class vector2_t>
	polygon(std::vector<vector2_t> const & vec)
	{
		setPointCount(vec.size());
		for (size_t i = 0; i < vec.size(); ++i)
		{
			setPoint(i, sf::Vector2f(vec[i].x,vec[i].y));
		}
	}
};

#endif
