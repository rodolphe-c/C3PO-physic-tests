// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2017 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include "hopp/container.hpp"
#include "hopp/geometry.hpp"

#include "geometry.hpp"
#include "mathematics.hpp"
#include "../quantum_mechanics.hpp"

namespace c3po
{
	template<class vector_t>
	inline void collision (size_t const & i, vector_t & entities)
	{
		for(std::size_t j = 0; j < i; j++)
		{
			if (hopp::geometry::overlap(entities[i].shape, entities[j].shape))
			{
				hopp::vector2<float> n = normalize (hopp::vector2<float>((entities[j].position().x - entities[i].position().x),(entities[j].position().y - entities[i].position().y)));

				auto mass = entities[i].mass + entities[j].mass;
				auto pos0 = mult(n, mass/entities[i].mass);
				entities[i].set_position(pos0.x + entities[i].position().x, pos0.y + entities[i].position().y);
				auto pos1 = mult(n, mass/entities[j].mass);
				entities[j].set_position(pos1.x + entities[j].position().x, pos1.y + entities[j].position().y);

				float  p = 2 * (entities[i].v.x * n.x + entities[i].v.y * n.y - entities[j].v.x * n.x - entities[j].v.y * n.y) / (entities[i].mass + entities[j].mass);

				entities[i].v = hopp::vector2<float>(entities[i].v.x - p * entities[j].mass * n.x,
	        						entities[i].v.y - p * entities[i].mass * n.y);
				entities[j].v = hopp::vector2<float>( entities[j].v.x + p * entities[i].mass * n.x,
									entities[j].v.y + p * entities[j].mass * n.y);
			}
		}
	}
}

inline
sf::Uint8 to_sfml(hopp::uint8 const & uint8)
{
	return sf::Uint8(uint8.i);
}

inline
sf::Color to_sfml(hopp::color const & color)
{
	return sf::Color(to_sfml(color.r), to_sfml(color.g), to_sfml(color.b), to_sfml(color.a));
}


#endif
