// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "hopp/random.hpp"
#include "hopp/geometry.hpp"
#include "hopp/container.hpp"
#include "hopp/color.hpp"


namespace c3po
{
	template <class T>
	class entity_circle
	{
	public:
		hopp::circle<T> shape;
		hopp::vector2<T> a;
		hopp::vector2<T> v;

		// Angular components
		T orientation; // radians
		T angularVelocity;
		T torque;

		T mass;
		T vol;
		T p;
		T rho;
		T q;

		hopp::color color_background;

		template<class vector2_t>
		entity_circle(T const diameter, vector2_t const & A, vector2_t const & B) :
		shape(A.x, A.y, diameter/2),
		a(0, 0),
		v((B.x - A.x)/10, (B.y - A.y)/10)
		{
			mass = T(hopp::random::uniform(1,10));
			q = T(hopp::random::uniform(-10,1990));
			rho = mass/vol;
			vol = 0.01f;

			std::cout << "mass: " << mass << std::endl;

			hopp::uint8 r (unsigned (hopp::random::uniform(0,255)));
			hopp::uint8 g (unsigned (hopp::random::uniform(0,255)));
			hopp::uint8 b (unsigned (hopp::random::uniform(0,255)));

			color_background = hopp::color(r,g,b);
		}

		hopp::vector2<T> position() const
		{
			return hopp::vector2<T>(shape.x, shape.y);
		}

		template <class vector2_t>
		void set_position(vector2_t const & p)
		{
			shape.x = p.x;
			shape.y = p.y;
		}

		void set_position(T const & x, T const & y)
		{
			set_position(hopp::vector2<T>(x,y));
		}

		T radius() const
		{
			return shape.radius;
		}
	};

	template <class T>
	class entity_rectangle
	{
	public:
		hopp::rectangle<T> shape;
		hopp::vector2<T> a;
		hopp::vector2<T> v;

		T mass;
		T vol;
		T p;
		T rho;
		T q;

		hopp::color color_background;

		template<class vector2_t>
		entity_rectangle(T const width, T const height, vector2_t const & A, vector2_t const & B) :
		shape(A.x, A.y, width, height),
		a(0, 0),
		v((B.x - A.x)/10, (B.y - A.y)/10)
		{
			mass = T(hopp::random::uniform(1,10));
			q = T(hopp::random::uniform(-10,1990));
			rho = mass/vol;
			vol = 0.01f;

			hopp::uint8 r (unsigned (hopp::random::uniform(0,255)));
			hopp::uint8 g (unsigned (hopp::random::uniform(0,255)));
			hopp::uint8 b (unsigned (hopp::random::uniform(0,255)));

			color_background = hopp::color(r,g,b);
		}

		hopp::vector2<T> position() const
		{
			return hopp::vector2<T>(shape.x, shape.y);
		}

		template <class vector2_t>
		void set_position(vector2_t const & p)
		{
			shape.x = p.x;
			shape.y = p.y;
		}

		void set_position(T const & x, T const & y)
		{
			set_position(hopp::vector2<T>(x,y));
		}

		T radius() const
		{
			return shape.radius;
		}
	};

	template <class T>
	class entity_polygon
	{
	public:

	};
}

#endif
