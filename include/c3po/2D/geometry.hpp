// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cmath>

#include <SFML/Graphics.hpp>

#include "polygon.hpp"

// http://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
template <typename T> inline constexpr
int signum(T x, std::false_type /* is_signed */)
{
	return T(0) < x;
}

template <typename T> inline constexpr
int signum(T x, std::true_type /* is_signed */)
{
	return (T(0) < x) - (x < T(0));
}

template <typename T> inline constexpr
int signum(T x)
{
	return signum(x, std::is_signed<T>());
}

// https://github.com/libgdx/libgdx/blob/master/gdx/src/com/badlogic/gdx/math/Intersector.java

/** Determines on which side of the given line the point is. Returns -1 if the point is on the left side of the line, 0 if the
	 * point is on the line and 1 if the point is on the right side of the line. Left and right are relative to the lines direction
	 * which is linePoint1 to linePoint2. */
inline
int pointLineSide (float linePoint1X, float linePoint1Y, float linePoint2X, float linePoint2Y, float pointX, float pointY)
{
	return signum((linePoint2X - linePoint1X) * (pointY - linePoint1Y) - (linePoint2Y - linePoint1Y) * (pointX - linePoint1X));
}

template <typename vector2_t> inline
bool overlapConvexPolygons (std::vector<vector2_t> const & p1, std::vector<vector2_t> const & p2)
{
	//return overlapConvexPolygons(verts1, 0, verts1.length, verts2, 0, verts2.length);
	float overlap = std::numeric_limits<float>::max();
	float smallest_axis_x = 0.f;
	float smallest_axis_y = 0.f;
	int numInNormalDir;

	// Get polygon1 axes
	for (size_t i = 0; i < p1.size(); ++i)
	{
		float x1 = p1[i].x;
		float y1 = p1[i].y;
		float x2 = p1[(i + 1) % p1.size()].x;
		float y2 = p1[(i + 1) % p1.size()].y;

		float axis_x = y1 - y2;
		float axis_y = -(x1 - x2);

		float const length = std::sqrt(axis_x * axis_x + axis_y * axis_y);
		axis_x /= length;
		axis_y /= length;

		// -- Begin check for separation on this axis --//

		// Project polygon1 onto this axis
		float min1 = axis_x * p1[0].x + axis_y * p1[0].y;
		float max1 = min1;
		for (size_t j = 0; j < p1.size(); ++j)
		{
			float p = axis_x * p1[j].x + axis_y * p1[j].y;
			if (p < min1) {min1 = p;}
			else if (p > max1) {max1 = p;}
		}

		// Project polygon2 onto this axis
		numInNormalDir = 0;
		float min2 = axis_x * p2[0].x + axis_y * p2[0].y;
		float max2 = min2;
		for (size_t j = 0; j < p2.size(); ++j)
		{
			// Counts the number of points that are within the projected area.
			numInNormalDir -= pointLineSide(x1, y1, x2, y2, p2[j].x, p2[j].y);
			float p = axis_x * p2[j].x + axis_y * p2[j].y;
			if (p < min2) {min2 = p;}
			else if (p > max2) {max2 = p;}
		}

		if (!((min1 <= min2 && max1 >= min2) || (min2 <= min1 && max2 >= min1))) {return false;}
		else
		{
			float o = std::min(max1, max2) - std::max(min1, min2);
			if ((min1 < min2 && max1 > max2) || (min2 < min1 && max2 > max1))
			{
				float mins = std::abs(min1 - min2);
				float maxs = std::abs(max1 - max2);
				if (mins < maxs) {o += mins;}
				else {o += maxs;}
			}
			if (o < overlap)
			{
				overlap = o;
				// Adjusts the direction based on the number of points found
				smallest_axis_x = numInNormalDir >= 0 ? axis_x : -axis_x;
				smallest_axis_y = numInNormalDir >= 0 ? axis_y : -axis_y;
			}
		}
		// -- End check for separation on this axis --//
	}

	// Get polygon2 axes
	for (size_t i = 0; i < p2.size(); ++i)
	{
		float x1 = p2[i].x;
		float y1 = p2[i].y;
		float x2 = p2[(i + 1) % p2.size()].x;
		float y2 = p2[(i + 1) % p2.size()].y;

		float axis_x = y1 - y2;
		float axis_y = -(x1 - x2);

		float const length = std::sqrt(axis_x * axis_x + axis_y * axis_y);
		axis_x /= length;
		axis_y /= length;

		// -- Begin check for separation on this axis --//
		numInNormalDir = 0;

		// Project polygon1 onto this axis
		float min1 = axis_x * p1[0].x + axis_y * p1[0].y;
		float max1 = min1;
		for (size_t j = 0; j < p1.size(); ++j)
		{
			float p = axis_x * p1[j].x + axis_y * p1[j].y;
			// Counts the number of points that are within the projected area.
			numInNormalDir -= pointLineSide(x1, y1, x2, y2, p1[j].x, p1[j].y);
			if (p < min1) {min1 = p;}
			else if (p > max1) {max1 = p;}
		}

		// Project polygon2 onto this axis
		float min2 = axis_x * p2[0].x + axis_y * p2[0].y;
		float max2 = min2;
		for (size_t j = 0; j < p2.size(); ++j)
		{
			float p = axis_x * p2[j].x + axis_y * p2[j].y;
			if (p < min2) {min2 = p;}
			else if (p > max2) {max2 = p;}
		}

		if (!((min1 <= min2 && max1 >= min2) || (min2 <= min1 && max2 >= min1)))
		{
			return false;
		}
		else
		{
			float o = std::min(max1, max2) - std::max(min1, min2);

			if ((min1 < min2 && max1 > max2) || (min2 < min1 && max2 > max1))
			{
				float mins = std::abs(min1 - min2);
				float maxs = std::abs(max1 - max2);
				if (mins < maxs) {o += mins;}
				else {o += maxs;}
			}

			if (o < overlap)
			{
				overlap = o;
				// Adjusts the direction based on the number of points found
				smallest_axis_x = numInNormalDir < 0 ? axis_x : -axis_x;
				smallest_axis_y = numInNormalDir < 0 ? axis_y : -axis_y;
			}
		}
		// -- End check for separation on this axis --//
	}
	return true;
}

/** Check whether specified counter-clockwise wound convex polygons overlap.
	 *
	 * @param p1 The first polygon.
	 * @param p2 The second polygon.
	 * @return Whether polygons overlap. */
inline
bool overlaps(polygon const & p1, polygon const & p2)
{
	std::vector<sf::Vector2f> transformed_vertices_1;
	auto m1 = p1.getTransform();
	for (size_t i = 0; i < p1.getPointCount(); ++i)
	{
		transformed_vertices_1.push_back(m1.transformPoint(p1.getPoint(i)));
	}

	std::vector<sf::Vector2f> transformed_vertices_2;
	auto m2 = p2.getTransform();
	for (size_t i = 0; i < p2.getPointCount(); ++i)
	{
		transformed_vertices_2.push_back(m2.transformPoint(p2.getPoint(i)));
	}

	return overlapConvexPolygons(transformed_vertices_1, transformed_vertices_2);
}

inline
bool overlaps (sf::CircleShape const & c1, sf::CircleShape const & c2)
{
	return std::pow(c1.getPosition().x - c2.getPosition().x, 2) + std::pow(c1.getPosition().y - c2.getPosition().y, 2) < std::pow(c1.getRadius() + c2.getRadius(), 2);
}
