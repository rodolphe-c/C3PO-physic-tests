// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2017 Théo Torcq, toto.rcq@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef MATHEMATICS_HPP
#define MATHEMATICS_HPP

namespace c3po
{
	template<class vector2_t = hopp::vector2<float>, class T = float>
	inline
	vector2_t mult (vector2_t const & v, T const k)
	{
		return vector2_t(v.x * k, v.y * k);
	}

	template<class vector2_t = hopp::vector2<float>, class T = float>
	inline
	auto dot (vector2_t const & v0, vector2_t const & v1) -> decltype(v0.x)
	{
		return v0.x * v1.x + v0.y * v1.y;
	}

	template<class vector2_t = hopp::vector2<float>>
	inline
	auto norm (vector2_t const & v) -> decltype(v.x)
	{
		return std::sqrt(dot(v,v));
	}

	template<class vector2_t = hopp::vector2<float>>
	inline
	vector2_t normalize(vector2_t const & v)
	{
		return mult(v,(1/norm(v)));
	}

	template<class vector2_t = hopp::vector2<float>>
	inline
	auto cross (vector2_t const & v0, vector2_t const & v1) -> decltype(v0.x)
	{
		return v0.x * v1.y - v0.y * v1.x;
	}

	template<class vector2_t = hopp::vector2<float>, class T = float>
	inline
	vector2_t cross(vector2_t const & a, T const & s)
	{
		vector2_t vec;
		vec.x = s * a.y;
		vec.y = -s * a.x;
		return vec;
	}

	template<class vector2_t = hopp::vector2<float>, class T = float>
	inline
	vector2_t cross(T const & s, vector2_t const & a)
	{
		vector2_t vec;
		vec.x = -s * a.y;
		vec.y = s * a.x;
		return vec;
	}
}

#endif
