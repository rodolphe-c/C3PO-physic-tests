#ifndef C3PO_HPP
#define C3PO_HPP

#include <string>

namespace c3po
{
	constexpr size_t version_major() { return 0; }
	constexpr size_t version_minor() { return 0; }
	constexpr size_t version_patch() { return 0; }

	inline std::string version() { return std::to_string(version_major())+"."+std::to_string(version_minor())+"."+std::to_string(version_patch()); }
	inline std::string codename() { return "Let's start physic !"; }
}

#endif
