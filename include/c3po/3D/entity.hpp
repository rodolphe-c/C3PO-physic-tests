// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef ENTITY_HPP
#define ENTITY_HPP

class entity
{
public:
	sf::RectangleShape rect;
	osg::Vec3 a;
	osg::Vec3 v;
	float masse;
	float vol;
	float p;
	float rho;
	float q;

	sf::Vector2f back_pos;
	sf::Color color;
	sf::Color color_background;

	std::vector<sf::Vertex> points;

	entity(sf::Vector2f const & A, sf::Vector2f const & B, float const size = 10.f) : rect(sf::Vector2f(size,size)), a(0, 0), v(5*(B.x - A.x), 5*(B.y - A.y)), color(sf::Uint8(std::rand()%255), sf::Uint8(std::rand()%255), sf::Uint8(std::rand()%255))
	{
		masse = float(std::rand()%10)+1.f;
		q = float(std::rand()%2000)-10.f;
		rho = masse/vol;
		vol = 0.01f;

		color_background = sf::Color(sf::Uint8(0), sf::Uint8(255 - masse*20), sf::Uint8(0));;

		rect.setPosition(A);
	}

	sf::Vector2f position()
	{
		return rect.getPosition();
	}
	void set_position(sf::Vector2f const & p)
	{
		rect.setPosition(p);
	}
	void set_position(float const & x, float const & y)
	{
		set_position(sf::Vector2f(x,y));
	}
};

#endif
