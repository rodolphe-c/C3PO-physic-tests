// Copyright © 2017 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#ifndef HOPP_GEOMETRY_CIRCLE_HPP
#define HOPP_GEOMETRY_CIRCLE_HPP

#include <iostream>


namespace hopp
{
	/**
	 * @brief Circle (x, y, radius)
	 *
	 * @code
	   #include <hopp/geometry.hpp>
	   @endcode
	 *
	 * @ingroup hopp_geometry
	 */
	template <class T>
	class circle
	{
	public:

		/// X coordinate
		T x;

		/// Y coordinate
		T y;

		/// Radius
		T radius;

		/// @brief Default constructor
		circle() : x(), y(), radius() { }

		/// @brief Constructor
		/// @param[in] x       X
		/// @param[in] y       Y
		/// @param[in] radius  Radius
		circle<T>(T const & x, T const & y, T const & radius) :
			x(x), y(y), radius(radius)
		{ }

		/// @brief Return left coordinate
		/// @return left
		T left() const { return x - radius; }

		/// @brief Return top coordinate
		/// @return top
		T top() const { return y - radius; }

		/// @brief Return right coordinate
		/// @return right
		T right() const { return x + radius; }

		/// @brief Return bottom coordinate
		/// @return bottom
		T bottom() const { return y + radius; }
	};

	/// @brief Operator << between a std::ostream and a hopp::circle<T>
	/// @param[in,out] out       A std::ostream
	/// @param[in]     circle    A hopp::circle<T>
	/// @return out
	/// @relates hopp::circle
	template <class T>
	std::ostream & operator <<(std::ostream & out, hopp::circle<T> const & circle)
	{
		out << "{ x = " << circle.x << ", y = " << circle.y << ", radius = " << circle.radius << " }";
		return out;
	}

	/// @brief Operator == between two hopp::circle<T>
	/// @param[in] a A hopp::circle<T>
	/// @param[in] b A hopp::circle<T>
	/// @return true if a == b, false otherwise
	/// @relates hopp::circle
	template <class T>
	bool operator ==(hopp::circle<T> const & a, hopp::circle<T> const & b)
	{
		return a.x == b.x && a.y == b.y && a.radius == b.radius;
	}

	/// @brief Operator != between two hopp::circle<T>
	/// @param[in] a A hopp::circle<T>
	/// @param[in] b A hopp::circle<T>
	/// @return true if a != b, false otherwise
	/// @relates hopp::circle
	template <class T>
	bool operator !=(hopp::circle<T> const & a, hopp::circle<T> const & b)
	{
		return (a == b) == false;
	}
}

#endif
